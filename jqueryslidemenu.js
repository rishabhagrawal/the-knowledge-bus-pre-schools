/*********************
//* jQuery Multi Level CSS Menu #2- By Dynamic Drive: http://www.dynamicdrive.com/
//* Last update: Nov 7th, 08': Limit # of queued animations to minmize animation stuttering
//* Menu avaiable at DD CSS Library: http://www.dynamicdrive.com/style/
*********************/

//Specify full URL to down and right arrow images (23 is padding-right to add to top level LIs with drop downs):
var arrowimages={down:['downarrowclass', '0', 50], right:['rightarrowclass', 'right.gif']}

var jqueryslidemenu={

animateduration: {over:600, out: 100}, //duration of slide in/ out animation, in milliseconds

buildmenu:function(menuid, arrowsvar){
	jQuery(document).ready(function($){
		var $mainmenu=$("#"+menuid+">ul")
		var $headers=$mainmenu.find("ul").parent()
		$headers.each(function(i){
			var $curobj=$(this)
			var $subul=$(this).find('ul:eq(0)')
			this._dimensions={w:this.offsetWidth, h:this.offsetHeight, subulw:$subul.outerWidth(), subulh:$subul.outerHeight()}
			this.istopheader=$curobj.parents("ul").length==1? true : false
			$subul.css({top:this.istopheader? this._dimensions.h+"px" : 0})
			$curobj.children("a:eq(0)").css(this.istopheader? {paddingRight: arrowsvar.down[2]} : {}).append(
				'<img src="'+ (this.istopheader? arrowsvar.down[1] : arrowsvar.right[1])
				+'" class="' + (this.istopheader? arrowsvar.down[0] : arrowsvar.right[0])
				+ '" style="border:0;" />'
			)
			$curobj.hover(
				function(e){
					var $targetul=$(this).children("ul:eq(0)")
					this._offsets={left:$(this).offset().left, top:$(this).offset().top}
					var menuleft=this.istopheader? 0 : this._dimensions.w
					menuleft=(this._offsets.left+menuleft+this._dimensions.subulw>$(window).width())? (this.istopheader? -this._dimensions.subulw+this._dimensions.w : -this._dimensions.w) : menuleft
					if ($targetul.queue().length<=1) //if 1 or less queued animations
						$targetul.css({left:menuleft+"px", width:this._dimensions.subulw+'px'}).slideDown(jqueryslidemenu.animateduration.over)
				},
				function(e){
					var $targetul=$(this).children("ul:eq(0)")
					$targetul.slideUp(jqueryslidemenu.animateduration.out)
				}
			) //end hover
		}) //end $headers.each()
		$mainmenu.find("ul").css({display:'none', visibility:'visible'})
	}) //end document.ready
}
}

//build menu with ID="myslidemenu" on page:
jqueryslidemenu.buildmenu("myslidemenu", arrowimages)







$("#residential").hover( function () {				
$(this).data('timeout', setTimeout( function () {	  
$("#residential_dropdown").show();  
 $("#residential_dropdown").animate({ height:"120px", width:"130px"  }, 100);
}, 300));
}, function () {

	$("#residential_dropdown").show(); 	
	$("#residential_dropdown").css("height","120px");	

  clearTimeout($(this).data('timeout'));


});


$("#residential").mouseleave( function(){
//$("#residential_dropdown").show();  
$("#commercial_dropdown").hide();  	


}
);





$("#commercial").hover( function () {				
$(this).data('timeout', setTimeout( function () {	  
$("#commercial_dropdown").show();  
 $("#commercial_dropdown").animate({ height:"120px", width:"160px"  }, 250);
}, 300));
}, function () {

	$("#commercial_dropdown").show(); 	
	$("#commercial_dropdown").css("height","120px");	

  clearTimeout($(this).data('timeout'));


});


$("#commercial").mouseleave( function(){
//$("#residential_dropdown").show();  
$("#residential_dropdown").hide();  	


}
);

$("#residential_dropdown").hover( function () {				
$(this).data('timeout', setTimeout( function () {	  
$("#residential_dropdown").show();  

}, 300));
}, function () {

	$("#residential_dropdown").show(); 	

  clearTimeout($(this).data('timeout'));


});


$("#residential_dropdown").mouseleave( function(){
//$("#residential_dropdown").show();  
$("#commercial_dropdown").hide();  	


}
);




	var height= screen.height;
	var container_height= screen.height - 410;
	document.getElementById("content_left").style.height=container_height+"px";
	
	alert(document.getElementById("content_left").style.height);



function showsearch()
{
		var q =	$("#field").val();
		
		if(q.toUpperCase().indexOf("HOMES")>=0 || q.toUpperCase().indexOf("HOME")>=0 || q.toUpperCase().indexOf("MODULAR")>=0 || q.toUpperCase().indexOf("RESIDENTIAL")>=0)
		{
		popitup1("http://absbnet.sytes.net/instanew/modular_homes.html");
		}	
			
		else if(q.toUpperCase().indexOf("VILLAS")>=0 || q.toUpperCase().indexOf("VILLA")>=0 )
		{
		popitup1("http://absbnet.sytes.net/instanew/villas.html");
		}			
		
		else if(q.toUpperCase().indexOf("CABINS")>=0 || q.toUpperCase().indexOf("CABIN")>=0 )
		{
		popitup1("http://absbnet.sytes.net/instanew/modern_cabin.html");
		}
		
		else if(q.toUpperCase().indexOf("ECO")>=0 || q.toUpperCase().indexOf("TENT")>=0 || q.toUpperCase().indexOf("COTTAGES")>=0 || q.toUpperCase().indexOf("COMMERCIAL")>=0 )
		{
			popitup1("http://absbnet.sytes.net/instanew/eco-tent_cottages.html");
		}
		
		else if(q.toUpperCase().indexOf("SAFARI")>=0 )
		{
			popitup1("http://absbnet.sytes.net/instanew/safari_tents.html");
		}
		
		else if(q.toUpperCase().indexOf("BEACH")>=0 )
		{
			popitup1("http://absbnet.sytes.net/instanew/beach_resorts.html");
		}
		
		else if(q.toUpperCase().indexOf("HI-TECH")>=0 || q.toUpperCase().indexOf("HITECH")>=0 || q.toUpperCase().indexOf("HI TECH")>=0 )
		{
			popitup1("http://absbnet.sytes.net/instanew/hi-tech_site_offices.html");
		}
		
		else if(q.toUpperCase().indexOf("NEWS")>=0 )
		{
			popitup1("http://absbnet.sytes.net/instanew/news_press.html");
		}
		
		else if(q.toUpperCase().indexOf("INSTA")>=0 || q.toUpperCase().indexOf("ABOUT")>=0 )
		{
			popitup1("http://absbnet.sytes.net/instanew/about_us.html");
		}
		
		else if(q.toUpperCase().indexOf("GALLERY")>=0 || q.toUpperCase().indexOf("PRODUCT")>=0 )
		{
			popitup1("http://absbnet.sytes.net/instanew/gallery.html");
		}
		
		else if(q.toUpperCase().indexOf("PROCESS")>=0 || q.toUpperCase().indexOf("PROJECT")>=0 )
		{
			popitup1("http://absbnet.sytes.net/instanew/process.html");
		}
		
		else if(q.toUpperCase().indexOf("CONTACT")>=0 || q.toUpperCase().indexOf("ADDRESS")>=0 )
		{
			popitup1("http://absbnet.sytes.net/instanew/contact_us.html");
		}
		
		else if(q.toUpperCase().indexOf("CATALOGUE")>=0 )
		{
			popitup1("http://absbnet.sytes.net/instanew/catalogue.html");
		}
		
		else{alert("No Result Found");}
}


function popitup1(url)
{
	newwindow=window.open(url, 'windowname', 'location=1, status=0, resizable=1, scrollbars=1, width=500, height=700');
	if (window.focus) {newwindow.focus()}
	return false;
}



