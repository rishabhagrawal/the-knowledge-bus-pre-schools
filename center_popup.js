function popupCenter(lnk, h, w)
{
	var scrWd = (screen.availWidth - w)/2;
	var scrHt = (screen.availHeight - h)/2;
	var prop = 'width=' + w + ',height=' + h + ',top=' + scrHt + ',left=' + scrWd + ',scrollbars=scroll, resize=no, menubar=no, toolbar=no';	
	window.open(lnk, null, prop);
}